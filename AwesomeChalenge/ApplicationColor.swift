//
//  ApplicationColor.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init?(hexString: String, alpha: CGFloat) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = hexString.substring(from: start)
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            } else if hexColor.count == 6 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                    g = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
                    b = CGFloat(hexNumber & 0x0000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: alpha)
                    return
                }
            }
        }
        
        return nil
    }
    
    static var blueText: UIColor {
        return UIColor(hexString: "00afd3", alpha: 1)!
    }
    
    static var blueBackground: UIColor {
        return UIColor(hexString: "#193044", alpha: 1)!
    }
    
    static var pinkButton: UIColor {
        return UIColor(hexString: "#FE3E6D", alpha: 1)!
    }
    
    static var greenText: UIColor {
        return UIColor(hexString: "#36cf6c", alpha: 1)!
    }
    
    static var redText: UIColor {
        return UIColor(hexString: "#f93131", alpha: 1)!
    }
    
    static var whiteText: UIColor {
        return UIColor(hexString: "#FFFFFF", alpha: 1)!
    }
    
    static var grayText: UIColor {
        return UIColor(hexString: "#888e93", alpha: 1)!
    }
}
