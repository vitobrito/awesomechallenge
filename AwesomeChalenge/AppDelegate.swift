//
//  AppDelegate.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 29/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let ordersViewController = OrdersViewController.storyboardInstance()
        window?.rootViewController = ordersViewController
        window?.makeKeyAndVisible()
        
        if AuthSessionRemoteService().token?.isEmpty != false {
            ordersViewController.presentLogin()
        }
        
        return true
    }

}

