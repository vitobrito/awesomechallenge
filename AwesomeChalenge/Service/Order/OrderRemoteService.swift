//
//  OrderRemoteService.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 29/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

class OrderRemoteService: OrderService, RemoteService {
    func fetchAll(completion: FetchAllCompletion?) {
        let path = APIPath.order(identifier: nil)
        networking.request(verb: .get, action: path.absolutePath(.v2), headers: path.header) { response in
            guard let json = response.result as? JSONDictionary else {
                completion?([], .apiError("Erro inesperado"))
                return
            }
            
            completion?(OrderListResponse(dictionary: json).orders.compactMap { $0.order }, nil)
        }
    }
    
    func fetchDetail(identifier: String, completion: FetchDetailCompletion?) {
        let path = APIPath.order(identifier: identifier)
        networking.request(verb: .get, action: path.absolutePath(.v2), headers: path.header) { response in
            guard let json = response.result as? JSONDictionary else {
                completion?(nil, .apiError("Erro inesperado"))
                return
            }
            
            completion?(OrderDetailResponse(dictionary: json).order, nil)
        }
    }
    
}
