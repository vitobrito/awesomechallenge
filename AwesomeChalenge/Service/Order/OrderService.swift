//
//  OrderService.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 29/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

typealias FetchAllCompletion = ([OrderObject], ErrorTypeApp?) -> Void
typealias FetchDetailCompletion = (OrderObject?, ErrorTypeApp?) -> Void


protocol OrderService {
    func fetchAll(completion: FetchAllCompletion?)
    func fetchDetail(identifier: String, completion: FetchDetailCompletion?)
}
