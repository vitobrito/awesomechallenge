//
//  RemoteService.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

protocol RemoteService { }

extension RemoteService {
    var apiRoot: APIRoot {
        return .sandbox
    }
    
    var networking: Networking {
        return Networking(url: URL(string: apiRoot.path)!, sslEnabled: true)
    }
}
