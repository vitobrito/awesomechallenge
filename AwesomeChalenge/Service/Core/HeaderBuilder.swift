//
//  HeaderBuilder.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

extension String {
    func base64() -> String {
        guard isEmpty == false else { return "" }
        
        return data(using: String.Encoding.utf8)?.base64EncodedString(options: []) ?? ""
    }
}

protocol HeaderBuilder {
    func build() -> [String : String]
}

class AuthHeaderBuilder: HeaderBuilder {
    private let user: String
    private let password: String
    
    init(user: String, password: String) {
        self.user = user
        self.password = password
    }
    
    private lazy var token: String = "\(user):\(password)".base64()
    
    func build() -> [String : String] {
        return [
            "Content-type" : "application/x-www-form-urlencoded",
            "Authorization" : "OAuth \(token)"
        ]
    }
}

class TokenizedHeaderBuilder: HeaderBuilder {
    
    let sessionService: SessionService
    
    init(sessionService: SessionService = LocalSessionService()) {
        self.sessionService = sessionService
    }
    
    func build() -> [String : String] {
        var dict = [
            "Content-type" : "application/json",
        ]
        
        if let token = sessionService.token {
            dict["Authorization"] = "OAuth \(token)"
        }
        
        return dict
    }
}


