//
//  API.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

enum APIRoot: String {
//    case production
    case sandbox
    
    var path: String {
        switch self {
//        case .production:
//            return ""
        case .sandbox:
            return "https://sandbox.moip.com.br/"
        }
    }
}

enum APIVersion: String {
    case v1
    case v2
    
    var path: String {
        return rawValue
    }
}

protocol Endpoint {
    var header: [String: String] { get }
    func absolutePath(_ version: APIVersion?) -> String
}

enum APIPath: Endpoint {
    case auth(user: String, password: String)
    case order(identifier: String?)
    
    var header: [String : String] {
        switch self {
        case .auth(user: let user, password: let password):
            return AuthHeaderBuilder(user: user, password: password).build()
        case .order:
            return TokenizedHeaderBuilder().build()
        }
    }
    
    fileprivate var path: String {
        switch self {
        case .auth: return "oauth/token"
        case .order(identifier: let identifier): return "orders".appendOnPathIfNotEmpty(parameter: identifier)
        }
    }
    
    func absolutePath(_ version: APIVersion?) -> String {
        let environment = APIRoot.sandbox
        
        let version = version?.path
        
        var root: String
        switch self {
        case .auth:
            root = "https://connect-sandbox.moip.com.br"
        case .order:
            root = environment.path
        }
        
        guard var rootURL = URL(string: root) else {
            assertionFailure("Invalid path: \(self)")
            return ""
        }
        
        if let version = version {
            rootURL.appendPathComponent(version)
        }
        
        rootURL.appendPathComponent(path)
        
        return rootURL.absoluteString
    }
}

private extension String {
    func appendOnPathIfNotEmpty(parameter: String?) -> String {
        guard let parameter = parameter, !parameter.isEmpty else { return self }
        
        return self + "/\(parameter)"
    }
}
