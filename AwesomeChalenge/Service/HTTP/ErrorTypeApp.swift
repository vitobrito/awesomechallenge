//
//  ErrorTypeApp.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

enum ErrorTypeApp: Error, Equatable {
    
    public static func == (lhs: ErrorTypeApp, rhs: ErrorTypeApp) -> Bool {
        return lhs.localizedDescription == rhs.localizedDescription
    }
    
    case parserError
    case networkError(String?)
    case noContent
    case apiError(String?)
    case invalidParam
    case notFound
    case expiredSession
    case unprocessableEntity
    case customError(Error)
    case internalServerError
    case forced
    case limitExceeded
    
    var localizedDescription: String {
        switch self {
        case .networkError(let message), .apiError(let message):
            return message ?? String(describing: self)
        case .limitExceeded:
            return "Limite de tentativas excedido. Tente novamente mais tarde. ERRO 01"
        default:
            return String(describing: self)
        }
    }
    
}
