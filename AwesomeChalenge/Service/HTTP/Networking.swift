//
//  Networking.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

enum HTTPVerb {
    case get
    case post
    case put
    case delete
    
    fileprivate var verbName: String {
        switch self {
        case .get:
            return "GET"
        case .post:
            return "POST"
        case .put:
            return "PUT"
        case .delete:
            return "DELETE"
        }
    }
    
    fileprivate var containsBody: Bool {
        return self == .post || self == .put
    }
}

struct HTTPResponse {
    let result: Any?
    let error: ErrorTypeApp?
    let rawData: Data?
    let headers: [AnyHashable : Any]
}

typealias JSONDictionary = [String : Any]

fileprivate extension String {
    func appendParametersAsQueryString(parameters: Any) -> String {
        var components = URLComponents(string: self)
        
        var items = [[URLQueryItem]]()
        
        if let json = parameters as? JSONDictionary {
            items = json.compactMap { param in
                if let array = param.value as? [AnyHashable] {
                    return array.compactMap { URLQueryItem(name: param.key, value: $0 as? String ?? $0.description) }
                }
                
                if let value = param.value as? AnyHashable {
                    return [URLQueryItem(name: param.key, value: value as? String ?? value.description)]
                }
                
                return nil
            }
        }
        
        let i = items.flatMap { $0 }
        
        if !i.isEmpty {
            components?.queryItems = i
        }
        
        return components?.url?.absoluteString.replacingOccurrences(of: "%5B", with: "[").replacingOccurrences(of: "%5D", with: "]") ?? self
    }
}

extension ErrorTypeApp {
    static func errorFromCode(code: Int) -> ErrorTypeApp? {
        switch code {
        case 200...299: return nil
        case 401: return .expiredSession
        case 404: return .notFound
        case 422: return .unprocessableEntity
        case 429: return .limitExceeded
        case 500: return .internalServerError
        default: return .apiError("")
        }
    }
}

class Networking: NSObject, URLSessionTaskDelegate, URLSessionDataDelegate {
    
    static var verbose = false
    
    private let dispatchGroup = DispatchGroup()
    private let domain: String
    private let globalSSLEnabled: Bool
    
    convenience init(url: URL, sslEnabled: Bool) {
        self.init(domain: url.absoluteString, sslEnabled: sslEnabled)
    }
    
    init(domain: String, sslEnabled: Bool) {
        self.domain = domain
        self.globalSSLEnabled = sslEnabled
    }
    
    private func baseURL(sslEnabled: Bool) -> URL? {
        let sslPrefix = "https://"
        let nonSslPrefix = "http://"
        
        let prefix = sslEnabled ? sslPrefix : nonSslPrefix
        
        let nonPrefixed = domain.replacingOccurrences(of: sslPrefix, with: "").replacingOccurrences(of: nonSslPrefix, with: "")
        
        return URL(string: prefix + nonPrefixed)
    }
    
    @discardableResult
    func request(verb: HTTPVerb,
                 action: String,
                 parameters: Any = JSONDictionary(),
                 headers: [String : String] = [:],
                 sslEnabled: Bool = false,
                 completion: @escaping (_ response: HTTPResponse) -> Void) -> URLSessionDataTask? {
        
        let sslEnabled = self.globalSSLEnabled || sslEnabled
        
        guard let request = buildRequest(verb: verb, action: action, parameters: parameters, sslEnabled: sslEnabled) else {
            return nil
        }
        
        headers.forEach { request.setValue($0.value, forHTTPHeaderField: $0.key) }
        
        let session = buildURLSession(sslEnabled: sslEnabled)
        
        conditionalLog(items: "REQUEST [\(verb.verbName)]")
        conditionalLog(items: "URL: \(request.url?.absoluteString ?? "[invalid]")")
        conditionalLog(items: "Headers: \(headers)")
        conditionalLog(items: "Parameters: \(parameters)")
        
        let queue = DispatchQueue(label: "com.evino.dispatchgroup", attributes: .concurrent, target: nil)
        
        dispatchGroup.enter()
        
        let task = session.dataTask(with: request as URLRequest) { data, response, error in
            
            self.conditionalLog(items: "RESPONSE: \(String(describing: response))")
            
            var headers: [AnyHashable : Any] = [:]
            
            if let response = response as? HTTPURLResponse {
                headers = response.allHeaderFields
            }
            
            if let response = response as? HTTPURLResponse, let appError = ErrorTypeApp.errorFromCode(code: response.statusCode) {
                switch appError {
                case .apiError(_):
                    if let d = data {
                        do {
                            let json = try JSONSerialization.jsonObject(with: d, options: []) as? JSONDictionary
                            let err = ErrorTypeApp.apiError(json?["message"] as? String)
                            completion(HTTPResponse(result: nil, error: err, rawData: data, headers: headers))
                        } catch {
                            completion(HTTPResponse(result: nil, error: appError, rawData: data, headers: headers))
                        }
                    } else {
                        completion(HTTPResponse(result: nil, error: appError, rawData: data, headers: headers))
                    }
                default:
                    completion(HTTPResponse(result: nil, error: appError, rawData: data, headers: headers))
                }
                return
            }
            
            if let d = data {
                do {
                    let deserialized = try JSONSerialization.jsonObject(with: d, options: [])
                    if let json =  deserialized as? JSONDictionary {
                        completion(HTTPResponse(result: json, error: nil, rawData: data, headers: headers))
                    } else if let jsonArray = deserialized as? [JSONDictionary] {
                        completion(HTTPResponse(result: jsonArray, error: nil, rawData: data, headers: headers))
                    } else {
                        completion(HTTPResponse(result: [:], error: nil, rawData: data, headers: headers))
                    }
                } catch {
                    if let response = response as? HTTPURLResponse, ErrorTypeApp.errorFromCode(code: response.statusCode) == nil {
                        completion(HTTPResponse(result: nil, error: nil, rawData: data, headers: headers))
                        return
                    }
                    
                    if let response = response as? HTTPURLResponse, let error = ErrorTypeApp.errorFromCode(code: response.statusCode) {
                        completion(HTTPResponse(result: nil, error: error, rawData: data, headers: headers))
                        return
                    } else {
                        completion(HTTPResponse(result: nil, error: .customError(error as NSError), rawData: data, headers: headers))
                    }
                }
                
            } else {
                completion(HTTPResponse(result: nil, error: .networkError(error?.localizedDescription), rawData: data, headers: headers))
            }
            
            session.finishTasksAndInvalidate()
        }
        
        queue.async(group: dispatchGroup) {
            task.resume()
            self.dispatchGroup.leave()
        }
        
        return task
    }
    // swiftlint:enable function_body_length
    
    private func buildURLSession(sslEnabled: Bool) -> URLSession {
        if !sslEnabled {
            return URLSession(configuration: .default)
        }
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        return session
    }
    
    private func buildRequest(verb: HTTPVerb, action: String, parameters: Any = JSONDictionary(), sslEnabled: Bool) -> NSMutableURLRequest? {
        let suffix = buildAction(verb: verb, action: action, parameters: parameters)
        guard let fullUrl = suffix.isEmpty ? baseURL(sslEnabled: sslEnabled) : URL(string: suffix, relativeTo: baseURL(sslEnabled: sslEnabled)) else {
            assert(false, "Invalid url/parameters")
            return nil
        }
        
        let request = NSMutableURLRequest(url: fullUrl)
        request.httpMethod = verb.verbName
        request.httpBody = buildBody(verb: verb, action: action, parameters: parameters)
        return request
    }
    
    private func buildAction(verb: HTTPVerb, action: String, parameters: Any) -> String {
        return verb.containsBody ? action : action.appendParametersAsQueryString(parameters: parameters)
    }
    
    private func buildBody(verb: HTTPVerb, action: String, parameters: Any) -> Data? {
        guard verb.containsBody else {
            return nil
        }
        
        guard let json = parameters as? JSONDictionary else {
            let data = ((parameters as? String) ?? "").data(using: .ascii)
            return data
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            return data
        } catch {
            return nil
        }
    }
    
    private func conditionalLog(items: Any...) {
        if Networking.verbose {
            print(items, terminator: "\n")
        }
    }
    
    // MARK: URLSessionDelegate
    internal func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard let protection = challenge.protectionSpace.serverTrust else {
            return
        }
        
        completionHandler(.useCredential, URLCredential(trust: protection))
    }
    
    internal func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        completionHandler(request as URLRequest)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard let protection = challenge.protectionSpace.serverTrust else {
            return
        }
        
        completionHandler(.useCredential, URLCredential(trust: protection))
    }
}

