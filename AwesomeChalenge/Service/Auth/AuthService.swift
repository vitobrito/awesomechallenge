//
//  AuthService.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 29/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

typealias AuthCompletion = (String?, ErrorTypeApp?) -> Void

protocol AuthService {
    func authenticate(user: String, password: String, completion: AuthCompletion?)
}
