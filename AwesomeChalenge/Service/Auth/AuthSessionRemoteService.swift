//
//  AuthSessionRemoteService.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

class AuthSessionRemoteService: AuthService, SessionService {

    private let sessionService: SessionService
    private let authService: AuthService
    
    var token: String? {
        get {
            return sessionService.token
        }
        set {
            sessionService.token = newValue
        }
    }
    
    init(sessionService: SessionService = LocalSessionService(), authService: AuthService = AuthRemoteService()) {
        self.authService = authService
        self.sessionService = sessionService
    }
    
    func authenticate(user: String, password: String, completion: AuthCompletion?) {
        authService.authenticate(user: user, password: password) { (token, error) in
            self.token = token
            completion?(token, error)
        }
    }
    
}
