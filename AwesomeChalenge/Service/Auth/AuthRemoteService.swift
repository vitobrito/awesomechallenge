//
//  AuthRemoteService.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 29/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

class AuthRemoteService: AuthService, RemoteService {
    func authenticate(user: String, password: String, completion: AuthCompletion?) {

        let request = AuthRequest(username: user, password: password)
        
        let path = APIPath.auth(user: user, password: password)
        
        networking.request(verb: .post, action: path.absolutePath(nil), parameters: request.urlEncoded() ?? "", headers: path.header) { response in
            guard let json = response.result as? JSONDictionary else {
                completion?(nil, response.error)
                return
            }
            
            completion?(AuthResponse(dictionary: json).accessToken, nil)
        }
    }
}
