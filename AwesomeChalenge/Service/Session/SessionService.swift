//
//  SessionService.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

protocol SessionService: class {
    var token: String? { get set }
}
