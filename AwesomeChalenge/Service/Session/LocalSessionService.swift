//
//  LocalSessionService.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

class LocalSessionService: SessionService {
    
    private let tokenDefaultsKey = "br.com.vbrittes.LocalSessionService"
    
    var token: String? {
        get {
            return UserDefaults.standard.object(forKey: tokenDefaultsKey) as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: tokenDefaultsKey)
        }
    }

}
