//
//  ArrayExtension.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 31/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

extension Array {
    func safeFind(at index: Int?) -> Element? {
        guard let index = index else {
            return nil
        }
        
        return index >= 0 && index < count ? self[index] : nil
    }
}
