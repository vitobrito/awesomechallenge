//
//  AuthObject.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 29/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit
import JSONHelper

struct AuthRequest {
    var clientId = "APP-H1DR0RPHV7SP"
    var clientSecret = "05acb6e128bc48b2999582cd9a2b9787"
    var grantType = "password"
    var username: String
    var password: String
    var deviceId = UUID().uuidString
    var scope = "APP_ADMIN"
    
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
    
    func urlEncoded() -> String? {
        var urlComponents = URLComponents()
        
        var items = [URLQueryItem]()
        items.append(URLQueryItem(name: "client_id", value: clientId))
        items.append(URLQueryItem(name: "client_secret", value: clientSecret))
        items.append(URLQueryItem(name: "grant_type", value: grantType))
        items.append(URLQueryItem(name: "username", value: username))
        items.append(URLQueryItem(name: "password", value: password))
        items.append(URLQueryItem(name: "device_id", value: deviceId))
        items.append(URLQueryItem(name: "scope", value: scope))
        
        urlComponents.queryItems = items
        
        return urlComponents.query
    }
}

struct AuthResponse: Deserializable {
    var accessToken: String?
    var id: String?
    
    init(dictionary: JSONDictionary) {
        accessToken <-- dictionary["accessToken"]
        let moipAccountJSON = dictionary["moipAccount"] as? JSONDictionary
        id <-- moipAccountJSON?["id"]
    }
}
