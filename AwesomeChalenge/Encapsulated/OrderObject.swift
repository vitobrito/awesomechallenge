//
//  OrderObject.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 29/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit
import JSONHelper

private extension DateFormatter {
    static var acceptedFormatters: [DateFormatter] {
        return ["yyyy-MM-dd'T'HH:mm:ss.sssz", "yyyy-MM-dd'T'HH:mm:ss.ssszz", "yyyy-MM-dd'T'HH:mm:ssZ"].map { format -> DateFormatter in
            let f = DateFormatter()
            f.dateFormat = format
            return f
        }
    }
    
    static func matchDateString(string: String) -> Date? {
        return acceptedFormatters.compactMap { $0.date(from: string) }.first
    }
}

//MARK: - enums

/// Contained in OrderResponse
enum PaymentMethod: String {
    case debitCard = "DEBIT_CARD"
    case boleto = "BOLETO"
    case onlineBankFinancing = "ONLINE_BANK_FINANCING"
    case onlineBankDebit = "ONLINE_BANK_DEBIT"
    case wallet = "WALLET"
}

/// Contained in OrderObject
enum Status: String {
    case waiting = "WAITING"
    case notPaid = "NOT_PAID"
    case paid = "PAID"
    case reverted = "REVERTED"
}

//MARK: - nestled structs

/// Contained in OrderListResponse
struct Summary: Deserializable {
    var count = 0
    var amount = 0
    
    init(dictionary: JSONDictionary) {
        count <-- dictionary["count"]
        amount <-- dictionary["amount"]
    }
}

/// Contained in OrderObject
struct Event: Deserializable {
    var type: PaymentMethod?
    var createdAt: Date?
    
    init(dictionary: JSONDictionary) {
        type <-- dictionary["type"]
        
        if let dateString = dictionary["createdAt"] as? String {
            createdAt = DateFormatter.matchDateString(string: dateString)
        }
    }
}

/// Contained in OrderObject
struct Customer: Deserializable {
    var fullname: String?
    var email: String?
    
    init(dictionary: JSONDictionary) {
        fullname <-- dictionary["fullname"]
        email <-- dictionary["email"]
    }
}

/// Contained in OrderObject
struct Amount: Deserializable {
    var total = 0
    
    init(dictionary: JSONDictionary) {
        total <-- dictionary["total"]
    }
}


/// Contained in OrderDetailResponse & OrdesListResponse
struct OrderObject: Deserializable {
    internal var amount: Amount?
    var total: Int {
        return amount?.total ?? 0
    }
    
    internal var events = [Event]()
    var updateDate: Date? {
        return events.first?.createdAt
    }
    
    var createdDate: Date?
    
    var status: Status?
    
    internal var customer: Customer?
    var customerEmail: String? {
        return customer?.email
    }
    var customerName: String? {
        return customer?.fullname
    }
    
    init(dictionary: JSONDictionary) {
        amount <-- dictionary["amount"]
        events <-- dictionary["events"]
        status <-- dictionary["status"]
        customer <-- dictionary["customer"]
        
        if let dateString = dictionary["createdAt"] as? String {
            createdDate = DateFormatter.matchDateString(string: dateString)
        }
    }
}

//MARK: - main structs

/// Maps response from OrdeRemoteService.fetchDetail
struct OrderDetailResponse: Deserializable {
    var order: OrderObject?
    
    init(dictionary: JSONDictionary) {
        order <-- dictionary
    }
}

/// Maps response from OrderRemoteService.fetchAll
struct OrderListResponse: Deserializable {
    
    var summary: Summary?
    var orders = [OrderDetailResponse]()
    
    init(dictionary: JSONDictionary) {
        summary <-- dictionary["summary"]
        orders <-- dictionary["orders"]
    }
}


