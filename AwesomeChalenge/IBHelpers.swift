//
//  IBHelpers.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

protocol IBDesigned {
    static var ibFileName: String { get }
}

extension IBDesigned where Self: UIViewController {
    static func storyboardInstance() -> Self {
        let storyboard = UIStoryboard(name: ibFileName, bundle: nil)
        let name = String(describing: Self.self)
        
        return storyboard.instantiateViewController(withIdentifier: name) as! Self
    }
}

extension IBDesigned where Self: UITableViewCell {
    static var ibFileName: String {
        return String(describing: Self.self)
    }
    
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }
    
    static func dequeueFromTableView(tableView: UITableView) -> Self {
        let cell = tableView.dequeueReusableCell(withIdentifier: Self.reuseIdentifier) as! Self
        
        return cell
    }
    
    static func registerForTable(tableView: UITableView) {
        tableView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
    }
}
