//
//  OrderDetailViewModel.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 31/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

protocol OrdersDetailDelegate: class {
    
}

class OrderDetailViewModel {
    weak var delegate: OrdersDetailDelegate?
    
    init(delegate: OrdersDetailDelegate?) {
        self.delegate = delegate
    }
}
