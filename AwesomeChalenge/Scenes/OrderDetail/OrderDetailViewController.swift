//
//  OrderDetailViewController.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    private func setupInterface() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }

}

extension OrderDetailViewController: IBDesigned {
    static var ibFileName: String {
        return "OrderDetail"
    }
}
