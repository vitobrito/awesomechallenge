//
//  OrderTableViewCell.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 31/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell, IBDesigned {
    
    @IBOutlet weak var thumbImageView: UIImageView!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInterface()
    }
    
    private func setupInterface() {
        backgroundColor = .white
        
        thumbImageView.backgroundColor = .yellow
        
        priceLabel.font = UIFont.boldSystemFont(ofSize: 14)
        
        statusLabel.font = UIFont.systemFont(ofSize: 14)
        
        emailLabel.font = UIFont.systemFont(ofSize: 14)
        
        timeLabel.font = UIFont.systemFont(ofSize: 14)
    }
    
    func populate(fill: OrderFill?) {
        priceLabel.text = fill?.formattedPrice
        statusLabel.text = fill?.formattedStatus
        emailLabel?.text = fill?.customer
        timeLabel.text = fill?.time
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        alpha = selected ? 0.8 : 1
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        alpha = highlighted ? 0.8 : 1
    }
}
