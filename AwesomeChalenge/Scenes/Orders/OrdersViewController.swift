//
//  OrdersViewController.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

class OrdersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var ordersDetailLabel: UILabel!
    
    private lazy var viewModel: OrdersViewModel = OrdersViewModel(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupInterface()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.fetchOrders()
    }
    
    private func setupInterface() {
        view.backgroundColor = UIColor.blueBackground
        
        OrderTableViewCell.registerForTable(tableView: tableView)
        
        var currentInset = tableView.contentInset
        currentInset.top = ordersDetailLabel.frame.maxY
        
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        tableView.contentInset = currentInset
        tableView.separatorStyle = .none
    }

}

extension OrdersViewController {
    func presentLogin() {
        let loginVc = LoginViewController.storyboardInstance()
        present(loginVc, animated: true, completion: nil)
    }
    
    func refresh() {
        viewModel.fetchOrders()
    }
}

extension OrdersViewController: OrdersDelegate {
    func updatedOrders() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension OrdersViewController: UITableViewDataSource, UITableViewDelegate {
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = OrderTableViewCell.dequeueFromTableView(tableView: tableView)
        cell.populate(fill: viewModel.orderFill(index: indexPath.row))
        
        return cell
    }
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.orderCount
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    internal func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if #available(iOS 11.0, *) {
            cell.layer.cornerRadius = 16
            
            let isFirst = indexPath.row == 0
            let isLast = indexPath.row == viewModel.orderCount - 1
            
            var cornersToRound = [CACornerMask]()
            
            if isFirst {
                cornersToRound.append(contentsOf: [.layerMaxXMinYCorner, .layerMinXMinYCorner])
            }
            
            if isLast {
                cornersToRound.append(contentsOf: [.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
            }
            
            cell.layer.maskedCorners = CACornerMask(cornersToRound)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension OrdersViewController: IBDesigned {
    static var ibFileName: String {
        return "Orders"
    }
}
