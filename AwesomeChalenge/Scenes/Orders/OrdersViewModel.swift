//
//  OrdersViewModel.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 31/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

struct OrderFill {
    var formattedPrice: String?
    var status: Status?
    var formattedStatus: String?
    var customer: String?
    var time: String?
    var paymentMethod: PaymentMethod?
}

protocol OrdersDelegate: class {
    func updatedOrders()
}

class OrdersViewModel {
    weak var delegate: OrdersDelegate?
    
    fileprivate var orders = [OrderObject]()
    fileprivate let orderService: OrderService
    
    init(delegate: OrdersDelegate?, orderService: OrderService = MockOrderService()) {
        self.delegate = delegate
        self.orderService = orderService
    }
    
    func fetchOrders() {
        orderService.fetchAll { (orders, error) in
            self.orders = orders
            self.delegate?.updatedOrders()
        }
    }
}

extension OrdersViewModel {
    var orderCount: Int {
        return orders.count
    }
    
    func orderFill(index: Int) -> OrderFill? {
        guard let order = orders.safeFind(at: index) else {
            return nil
        }
        
        return OrderFill(formattedPrice: "R$ 99,99", status: order.status, formattedStatus: order.status?.rawValue, customer: order.customerEmail, time: "1 dia atrás", paymentMethod: .debitCard)
    }
}
