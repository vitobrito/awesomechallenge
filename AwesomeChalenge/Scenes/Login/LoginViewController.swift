//
//  LoginViewController.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var userContainerView: UIView!
    @IBOutlet weak var userTitleLabel: UILabel!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var userIconImageView: UIImageView!
    
    @IBOutlet weak var passwordContainerView: UIView!
    @IBOutlet weak var passwordTitleLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordIconImageView: UIImageView!
    @IBOutlet weak var seePasswordButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!
    
    fileprivate lazy var viewModel: LoginViewModel = LoginViewModel(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupInterface()
        setupActions()
    }
    
    private func setupInterface() {
        view.backgroundColor = UIColor.blueBackground
        
        userContainerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        userContainerView.layer.cornerRadius = 16
        
        userTitleLabel.text = "Nome ou email"
        
        userTextField.keyboardAppearance = .dark
        userTextField.isHidden = true
        userTextField.delegate = self
        userTextField.textColor = UIColor.whiteText
        
        passwordContainerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        passwordContainerView.layer.cornerRadius = 16
        
        passwordTitleLabel.text = "Senha"
        
        passwordTextField.keyboardAppearance = .dark
        passwordTextField.isSecureTextEntry = true
        passwordTextField.isHidden = true
        passwordTextField.delegate = self
        passwordTextField.textColor = UIColor.whiteText
        
        errorLabel.textColor = UIColor.pinkButton
        errorLabel.text = nil
        
        loginButton.backgroundColor = UIColor.pinkButton
        loginButton.layer.cornerRadius = 16
        loginButton.setTitleColor(UIColor.whiteText, for: .normal)
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        
        forgotButton.setTitleColor(UIColor.whiteText, for: .normal)
        forgotButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        
    }

    private func setupActions() {
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.userContainerTapAction))
        userContainerView.isUserInteractionEnabled = true
        userContainerView.addGestureRecognizer(tapGesture)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.passwordContainerTapAction))
        passwordContainerView.isUserInteractionEnabled = true
        passwordContainerView.addGestureRecognizer(tapGesture)
        
        seePasswordButton.addTarget(self, action: #selector(LoginViewController.seePasswordAction), for: .touchUpInside)
        
        loginButton.addTarget(self, action: #selector(LoginViewController.loginAction), for: .touchUpInside)
    }
    
    private func setupAutomationIds() {
        userTextField.accessibilityIdentifier = "userTextField"
        passwordTextField.accessibilityIdentifier = "passwordTextField"
        
        loginButton.accessibilityIdentifier = "loginButton"
    }
    
}

extension LoginViewController: LoginDelegate {
    func authenticationSuccess() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func authenticationError(message: String) {
        DispatchQueue.main.async {
            self.errorLabel.text =  message
        }
    }
}

extension LoginViewController {

    @objc func userContainerTapAction() {
        userTextField.becomeFirstResponder()
    }
    
    @objc func passwordContainerTapAction() {
        passwordTextField.becomeFirstResponder()
    }
    
    @objc func seePasswordAction() {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
    
    @objc func loginAction() {
        view.resignFirstResponder()
        viewModel.authenticate()
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.isHidden = textField.text?.isEmpty != false
        
        errorLabel.text = nil
        
        switch textField {
        case userTextField:
            viewModel.validateUser(user: textField.text)
        case passwordTextField:
            viewModel.validatePassword(password: textField.text)
        default: break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.isHidden = false
    }
}

extension LoginViewController: IBDesigned {
    static var ibFileName: String {
        return "Login"
    }
}
