//
//  LoginViewModel.swift
//  AwesomeChalenge
//
//  Created by Victor Brittes on 31/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit

protocol LoginDelegate: class {
    func authenticationSuccess()
    func authenticationError(message: String)
}

class LoginViewModel {
    weak var delegate: LoginDelegate?
    
    private let authService: AuthService
    
    private(set) var user: String?
    private(set) var password: String?

    init(delegate: LoginDelegate?, authService: AuthService = AuthSessionRemoteService()) {
        self.delegate = delegate
        self.authService = authService
    }
}

extension LoginViewModel {
    @discardableResult
    func validateUser(user: String?) -> Bool {
        guard let user = user, !user.isEmpty else {
            self.user = nil
            self.delegate?.authenticationError(message: "Usuário vazio")
            return false
        }
        
        self.user = user
        
        return true
    }
    
    @discardableResult
    func validatePassword(password: String?) -> Bool {
        guard let password = password, !password.isEmpty else {
            self.password = nil
            self.delegate?.authenticationError(message: "Senha vazia")
            return false
        }
        
        self.password = password
        
        return true
    }
    
    func authenticate() {
        if !validateUser(user: user) {
            return
        }
        
        if !validatePassword(password: password) {
            return
        }
        
        authService.authenticate(user: user ?? "", password: password ?? "") { (_, error) in
            guard let _ = error else {
                self.delegate?.authenticationSuccess()
                return
            }
            
            self.delegate?.authenticationError(message: "Login ou senha inválidos")
        }
    }
}
