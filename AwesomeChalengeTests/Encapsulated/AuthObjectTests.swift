//
//  AuthObjectTests.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import XCTest
@testable import AwesomeChalenge

class AuthObjectTests: XCTestCase {
    func testDeserialization() {
        let json = JSONReader().jsonFromFile(name: "AuthResponse")
        
        let auth = AuthResponse(dictionary: json)
        
        XCTAssertEqual(auth.accessToken, "0bb4be77d23946f2aabde7d9f824ffd2_v2")
        XCTAssertEqual(auth.id, "MPA-C4DD7D666638")
    }
}
