//
//  OrderObjectTests.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import XCTest
@testable import AwesomeChalenge

class OrderObjectTests: XCTestCase {
    
    func testOrderDetailDeserialization() {
        let json = JSONReader().jsonFromFile(name: "OrderDetailResponse")
        
        let orderResponse = OrderDetailResponse(dictionary: json)
        
        let order = orderResponse.order
        
        XCTAssertNotNil(order)
        
        XCTAssertEqual(order?.total, 4650)
        
        XCTAssertEqual(order?.events.count, 3)
        XCTAssertEqual(String(describing: order?.updateDate ?? Date()), "2018-08-29 01:20:00 +0000")
        
        XCTAssertEqual(String(describing: order?.createdDate ?? Date()), "2018-08-10 13:39:00 +0000")
        
        XCTAssertEqual(order?.status, Status.reverted)
        
        XCTAssertNotNil(order?.customer)
        XCTAssertEqual(order?.customerName, "Jessica Andrade")
        XCTAssertEqual(order?.customerEmail, "jessica@email.com")
    }
    
    func testOrderListDeserialization() {
        let json = JSONReader().jsonFromFile(name: "AllOrdersResponse")
        
        let orderResponse = OrderListResponse(dictionary: json)
        
        let orders = orderResponse.orders
        
        XCTAssertEqual(orderResponse.orders.count, 20)
        
        let order = orders.first?.order
        
        XCTAssertNotNil(order)
        
        XCTAssertEqual(order?.total, 4650)
        
        XCTAssertEqual(order?.events.count, 1)
        XCTAssertEqual(String(describing: order?.updateDate ?? Date()), "2018-08-10 10:40:04 +0000")
        
        XCTAssertEqual(String(describing: order?.createdDate ?? Date()), "2018-08-10 13:39:43 +0000")
        
        XCTAssertEqual(order?.status, Status.reverted)
        
        XCTAssertNotNil(order?.customer)
        XCTAssertEqual(order?.customerName, "jessica andrade")
        XCTAssertEqual(order?.customerEmail, "jessica@email.com")
    }
}
