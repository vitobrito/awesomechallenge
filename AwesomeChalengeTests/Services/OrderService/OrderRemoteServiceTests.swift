//
//  OrderRemoteServiceTests.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import XCTest
@testable import AwesomeChalenge

class OrderRemoteServiceTests: XCTestCase {

    override func setUp() {
        let e = easyExpectation()
        
        AuthSessionRemoteService().authenticateValidUser { (_, _) in
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }

    func testFetchAll() {
        let orderService = OrderRemoteService()
        
        let e = easyExpectation()
        
        orderService.fetchAll { (orders, error) in
            XCTAssertNil(error)
            XCTAssertFalse(orders.isEmpty)
            
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testFetchDetail() {
        let orderService = OrderRemoteService()
        
        let e = easyExpectation()
        
        orderService.fetchDetail(identifier: "ORD-VOWBGC4Z7FVS") { (order, error) in
            XCTAssertNotNil(order)
            XCTAssertNil(error)
            
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }

}
