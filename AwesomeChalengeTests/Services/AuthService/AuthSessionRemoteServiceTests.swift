//
//  AuthSessionRemoteServiceTests.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import XCTest
@testable import AwesomeChalenge

extension AuthSessionRemoteService {
    func authenticateValidUser(completion: AuthCompletion?) {
        authenticate(user: "moip-test-developer@moip.com.br", password: "testemoip123", completion: completion)
    }
}

class AuthSessionRemoteServiceTests: XCTestCase {

    func testAuthenticateSuccess() {
        let e = easyExpectation()
        
        let authService = AuthSessionRemoteService()
        
        authService.authenticateValidUser { (token, error) in
            XCTAssertTrue(token?.isEmpty == false)
            XCTAssertNil(error)
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
        
        XCTAssertNotNil(authService.token)
    }
    
    func testAuthenticateFailure() {
        let e = easyExpectation()
        
        let authService = AuthSessionRemoteService()
        
        authService.authenticate(user: "moip-test-developer@moip.com.br", password: "abc123") { (token, error) in
            XCTAssertTrue(token?.isEmpty != false)
            XCTAssertNotNil(error)
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
        
        XCTAssertNil(authService.token)
    }

}
