//
//  AuthRemoteServiceTests.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import XCTest
@testable import AwesomeChalenge

class AuthRemoteServiceTests: XCTestCase {
    func testAuthenticateSuccess() {
        let e = easyExpectation()
        
        let authService = AuthRemoteService()
        
        authService.authenticate(user: "moip-test-developer@moip.com.br", password: "testemoip123") { (token, error) in
            XCTAssertTrue(token?.isEmpty == false)
            XCTAssertNil(error)
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testAuthenticateFailure() {
        let e = easyExpectation()
        
        let authService = AuthRemoteService()
        
        authService.authenticate(user: "moip-test-developer@moip.com.br", password: "abc123") { (token, error) in
            XCTAssertTrue(token?.isEmpty != false)
            XCTAssertNotNil(error)
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
}
