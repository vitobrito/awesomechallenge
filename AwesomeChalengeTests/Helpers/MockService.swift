//
//  MockService.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit
@testable import AwesomeChalenge

class JSONReader {
    func jsonFromFile(name: String) -> JSONDictionary {
        let path = Bundle.allBundles.compactMap { $0.path(forResource: name, ofType: "json")}.first
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        let jsonResult = try! JSONSerialization.jsonObject(with: data, options: []) as! JSONDictionary
        
        return jsonResult
    }
    
    func jsonArrayFromFile(name: String) -> [JSONDictionary] {
        let path = Bundle.allBundles.compactMap { $0.path(forResource: name, ofType: "json")}.first
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        let jsonResult = try! JSONSerialization.jsonObject(with: data, options: []) as! [JSONDictionary]
        
        return jsonResult
    }
}

protocol MockService { }

extension MockService {
    func jsonFromFile(name: String) -> JSONDictionary {
        return JSONReader().jsonFromFile(name: name)
    }
    
    func jsonArrayFromFile(name: String) -> [JSONDictionary] {
        return JSONReader().jsonArrayFromFile(name: name)
    }
}
