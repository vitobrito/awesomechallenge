//
//  XCTestCaseExtension.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import XCTest

extension XCTestCase {
    func easyExpectation(line: Int = #line, file: String = #file) -> XCTestExpectation {
        return expectation(description: "easyExpectation:\(file):\(line)")
    }
}
