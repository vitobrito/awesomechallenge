//
//  AuthHeaderBuilderTests.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import XCTest
@testable import AwesomeChalenge

class AuthHeaderBuilderTests: XCTestCase {

    func testBuild() {
        let builder = AuthHeaderBuilder(user: "moip-test-developer@moip.com.br", password: "testemoip123")
        let header = builder.build()

        XCTAssertEqual(header.keys.count, 2)
        XCTAssertEqual(header["Authorization"], "OAuth bW9pcC10ZXN0LWRldmVsb3BlckBtb2lwLmNvbS5icjp0ZXN0ZW1vaXAxMjM=")
        XCTAssertEqual(header["Content-type"], "application/x-www-form-urlencoded")
    }

}
