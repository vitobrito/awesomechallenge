//
//  TokenizedHeaderBuilderTests.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 30/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import XCTest
@testable import AwesomeChalenge

class TokenizedHeaderBuilderTests: XCTestCase {

    func testBuild() {
        let builder = TokenizedHeaderBuilder()
        let header = builder.build()
        
        XCTAssertEqual(header.keys.count, 2)
        XCTAssertEqual(header["Content-type"], "application/json")
    }

}
