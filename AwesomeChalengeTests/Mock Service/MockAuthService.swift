//
//  MockAuthService.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 31/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit
@testable import AwesomeChalenge

class MockAuthService: AuthService, MockService {
    func authenticate(user: String, password: String, completion: AuthCompletion?) {
        DispatchQueue.global().async {
            let json = JSONReader().jsonFromFile(name: "AuthResponse")
            
            let auth = AuthResponse(dictionary: json)
            
            completion?(auth.accessToken, nil)
        }
    }
}
