//
//  MockOrderService.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 31/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import UIKit
@testable import AwesomeChalenge

class MockOrderService: OrderService, MockService {
    
    func fetchAll(completion: FetchAllCompletion?) {
        let json = JSONReader().jsonFromFile(name: "AllOrdersResponse")
        let orderResponse = OrderListResponse(dictionary: json)
        let orders = orderResponse.orders
        
        completion?(orders.compactMap { $0.order }, nil)
    }
    
    func fetchDetail(identifier: String, completion: FetchDetailCompletion?) {
        let json = JSONReader().jsonFromFile(name: "OrderDetailResponse")
        let orderResponse = OrderDetailResponse(dictionary: json)
        let order = orderResponse.order
        
        completion?(order, nil)
    }

}
