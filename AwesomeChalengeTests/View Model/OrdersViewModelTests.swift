//
//  OrdersViewModelTests.swift
//  AwesomeChalengeTests
//
//  Created by Victor Brittes on 31/07/19.
//  Copyright © 2019 Awesome. All rights reserved.
//

import XCTest
@testable import AwesomeChalenge

class OrdersMockDelegate: OrdersDelegate {
    
    var expectation: XCTestExpectation?
    
    func updatedOrders() {
        expectation?.fulfill()
    }
}

class OrdersViewModelTests: XCTestCase {

    var mockedViewModel = OrdersViewModel(delegate: nil, orderService: MockOrderService())
    
    func testOrdersFetching() {
        let viewModel = mockedViewModel
        
        XCTAssertEqual(viewModel.orderCount, 0)
        
        let mockDelegate = OrdersMockDelegate()
        mockDelegate.expectation = easyExpectation()
        viewModel.delegate = mockDelegate
        
        viewModel.fetchOrders()
        
        waitForExpectations(timeout: 10, handler: nil)
        
        XCTAssertEqual(viewModel.orderCount, 20)
        
        let firstFill = viewModel.orderFill(index: 0)
        
        XCTAssertEqual(firstFill?.customer, "jessica@email.com")
//        XCTAssertEqual(firstFill?.formattedPrice, "")
//        XCTAssertEqual(firstFill?.formattedStatus, "")
        XCTAssertEqual(firstFill?.paymentMethod, .debitCard)
        XCTAssertEqual(firstFill?.status, .reverted)
//        XCTAssertEqual(firstFill?.time, "")
    }
    
}
